import React, { Component } from 'react';
import Reflux from 'reflux';
import axios from 'axios';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

import mapboxgl from 'mapbox-gl';

mapboxgl.accessToken = 'pk.eyJ1IjoibGdiYW51ZWxvcyIsImEiOiJjanRwMDUzOGMwNHE0NDlwYjUyd3YyMWc5In0.OiKOZbsVyVfHjXBYMzwfdg';

let Actions = Reflux.createActions(['increment', 'decrement', {'submit':{children: ['completed', 'failed']}}]);

Actions.submit.listen(function() {
  var that = this;
  axios.post('http://localhost:4000/api/bookings', {pickup_address: 'ITEM Puebla', dropoff_address: 'Plaza Dorada'})
    .then((res) => that.completed(res.data))
    .catch(that.failed)
});

class AppStore extends Reflux.Store {
  constructor() {
    super();
    this.state = {value: 0, syncMessage: ""};
    this.listenables = Actions;
  }

  increment() {
    this.setState({value: this.state.value + 1});
  }
  decrement() {
    this.setState({value: this.state.value - 1});
  }
  onSubmitCompleted(data) {
    this.setState({syncMessage: data.msg});
  }
  onSubmitFailed() {
    console.log("ooops");
  }
}

class App extends Reflux.Component {
  constructor(props) {
    super(props);
    this.store = AppStore;
  }

  render() {
    return (
      <div>
        <div>
          <div>
            {this.state.value}
          </div>
          <div>
            {this.state.syncMessage}
          </div>
          <TextField
            label="Drop-off address"
            id="margin-none"
            helperText="Where are you going today?"
          />

          <Button variant="contained" color="primary">
            Hello World
          </Button>
          <button onClick={()=>Actions.submit()}>Submit booking</button>
          <button onClick={()=>Actions.increment()}>+</button>
          <button onClick={()=>Actions.decrement()}>-</button>
        </div>
      </div>
    );
  }
}

class Map extends Component {
  componentDidMount() {
    navigator.geolocation.getCurrentPosition(position => {
      console.log(position);

      this.map = new mapboxgl.Map({
        container: this.mapContainer,
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [position.coords.longitude, position.coords.latitude], // starting position [lng, lat]
        zoom: 14 // starting zoom
      });

      new mapboxgl.Marker()
        .setLngLat([position.coords.longitude, position.coords.latitude])
        .addTo(this.map);
    });
  }

  render() {
    const style = {
      position: 'absolute',
      top: 90,
      bottom: 0,
      width: '100%'
    };
    return (
      <div ref={el => this.mapContainer = el} style={style}/>
    )
  }
}

export {App, Map};
